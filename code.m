id copyMessageAcknowledgeMessage(id self, SEL _cmd, long long response, NSData* messageId, id token) {
    if (self._data == 0) {
        message = [[APSProtocolMessage alloc] initWithCommand:0xb];
    } else {
        message = 0;
    }

    [self._encoderWrapper setCommand:0xb];
    [message appendOneByteItem:0x8 byte:response];
    [self._encoderWrapper addInt8WithAttributeId:0x8 number:response isIndexable:0x0];
    if (messageId != 0) {
        [message appendItem:0x4 data:messageId];
        uint32_t messageIdNum = *(uint32_t*)[messageId bytes];
        [self._encoderWrapper addInt32WithAttributeId:0x4 number:messageIdNum isIndexable:0x0];
    }

    if (token != 0) {
        [message appendItem:0x1 data:token];
        [self._encoderWrapper addDataWithAttributeId:0x1 data:token isIndexable:0x1];
    }

    if (self._data == 0) {
        result = [message copyMessageData];
    } else {
        result = [self._encoderWrapper copyMessage];
    }

    [message release];
    return result;
}
